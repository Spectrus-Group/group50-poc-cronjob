
'use strict'

const URL='http://159.89.166.98:8020/';
const axios= require('axios')
const appendQuery= require('append-query');
const postBinary = (endpoint, data) => {

  console.log('posting now')
    console.log(URL+endpoint)
  return axios.post(URL+endpoint,data,{headers: {'Content-Type': 'application/octet-stream'}});
};


const getInfo = (endpoint) => {
    return axios.get(URL+endpoint);
};

const getRecords = (endpoint='records',query={}) => {
    let endpointwithQueryParam = appendQuery(endpoint,query)
    return axios.get(URL+endpointwithQueryParam);
};

const getAgents = (endpoint='agents',query={}) => {

    let endpointwithQueryParam = appendQuery(endpoint,query)
    return axios.get(URL+endpointwithQueryParam);

};
module.exports = {
  postBinary,
    getInfo,
    getRecords,
    getAgents
}
