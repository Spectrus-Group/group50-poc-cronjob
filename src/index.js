
'use strict'


const payloads = require('./payloads')
const transactions = require('./transactions')
const api= require('./api')
const parsing=require('./parsing');
const apiRoot='/api';
const apiVersion='v1';

/*
function to update a location property of an asset
*/

const checkPrivateKey = (req,res,next)=>{
    console.log('checking private key')
    if(!req.body.privateKey){
        return res.status(400).send({message:'Private key should be passed'});
    }

    return next();
};


const express = require('express');
const bodyParser=require('body-parser');
const app = new express();
const PORT=3000;
app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({extended:true}));

app.get(apiRoot+'/'+apiVersion+'/records',async(req,res)=>{
    try {
        let query= Object.assign({},req.query);
        let response= await api.getRecords('records',query);
        return res.send(response.data);
    }

    catch (err){
        res.status(400).send(err);
    }
})

app.get(apiRoot+'/'+apiVersion+'/agents',async(req,res)=>{
    try {
        let query= Object.assign({},req.query);
        let response= await api.getRecords('agents',query);
        return res.send(response.data);
    }

    catch (err){
        res.status(400).send(err);
    }
})
app.post(apiRoot+'/'+apiVersion+'/asset',checkPrivateKey,async (req,res)=>{
    const data =req.body || {};
    if(!data.assetId){
        return res.status(400).send({message:'asset id  is invalid'});
    }
    if(!data.type){
        return res.status(400).send({message:'type of asset is invalid'});
    }
    if(!data.subtype){
        return res.status(400).send({message:'subtype of asset is invalid'});
    }

    const properties = [];

    properties.push({
        name: 'type',
        stringValue: data.type,
        dataType: payloads.createRecord.enum.STRING
    })

    properties.push({
        name: 'subtype',
        stringValue: data.subtype,
        dataType: payloads.createRecord.enum.STRING
    })
    if (data.temperature) {
        properties.push({
            dataType:payloads.updateProperties.enum.INT,
            intValue:data.temperature,
            name:"temperature"
        })

    }
    if (data.humidity) {
        properties.push({
            dataType:payloads.updateProperties.enum.INT,
            intValue:data.humidity,
            name:"humidity"
        })

    }
    if (data.latitude && data.longitude) {
        properties.push({
            name: 'location',
            locationValue: {
                latitude: parsing.toInt(state.latitude),
                longitude: parsing.toInt(state.longitude)
            },
            dataType: payloads.createRecord.enum.LOCATION
        })
    }

    const recordPayload = payloads.createRecord({
        recordId: data.assetId,
        recordType: 'asset',
        properties
    })

    let reporterPayloads=[];
    if(data.reporter){
        reporterPayloads.push(payloads.createProposal({
            recordId: data.assetId,
            receivingAgent: data.reporter,
            role: payloads.createProposal.enum.REPORTER,
            properties: reporter.properties
        }))
    }
    try{

        let result= await transactions.submit([recordPayload].concat(reporterPayloads), true, data.privateKey);
        return res.status(200).send(result);

    }
    catch (err){
        console.log(err)
        return res.status(400).send(err);
    }

})


app.post(apiRoot+'/'+apiVersion+'/updateProperties',checkPrivateKey,async (req,res)=>{
    console.log('private key check passed');
    let data= req.body;
    let payload=[];
    console.log(data)
    if(!data.assetId){
        return res.status(400).send({message:'asset id is not given'});
    }

    if(data.latitude &&  data.longitude){
        let encodedPayload = payloads.updateProperties({
            recordId: data.assetId,
            properties: [{
                dataType:payloads.updateProperties.enum.LOCATION,
                locationValue:{
                    latitude: parsing.toInt(data.latitude),
                    longitude: parsing.toInt(data.longitude)
                },
                name:"location"
            }]
        });
        payload.push(encodedPayload);
    }

    if(data.temperature){


        let encodedPayload = payloads.updateProperties({
            recordId: data.assetId,
            properties: [{
                dataType:payloads.updateProperties.enum.INT,
                intValue:parsing.toInt(data.temperature),
                name:"temperature"}]
        });
        payload.push(encodedPayload);
    }

    if(data.humidity){


        let encodedPayload = payloads.updateProperties({
            recordId: data.assetId,
            properties: [{
                dataType:payloads.updateProperties.enum.INT,
                intValue:parsing.toInt(data.humidity),
                name:"humidity"}]
        });
        payload.push(encodedPayload);
    }

    console.log('length of payload is', payload.length)
    if(!payload.length){
        return res.send({});
    }
    try{

        console.log('submitting txn')
        let result= await transactions.submit(payload, true,data.privateKey);
         console.log(result)
        return res.status(200).send(result);

    }
    catch (err){
        console.log(err)
        return res.status(400).send(err );
    }


})

app.use((err,req,res,next)=>{
    res.send(err);
})

app.listen(PORT,()=>{
    console.log('listening on port', PORT);
})


