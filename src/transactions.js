
'use strict'

const _ = require('lodash')
const sjcl = require('sjcl')
const { createHash } = require('crypto')
const secp256k1 = require('sawtooth-sdk/signing/secp256k1')
const {
  Transaction,
  TransactionHeader,
  TransactionList
} = require('sawtooth-sdk/protobuf')
const api = require('./api')

const STORAGE_KEY = 'asset_track.encryptedKey'
const FAMILY_NAME = 'supply_chain'
const FAMILY_VERSION = '1.0'
const NAMESPACE = '3400de'

const context = new secp256k1.Secp256k1Context()
let privateKeyHex = '9f60816547ad05d7f7dc32272f7d695c6731f3fc7bbad513022e8920ecd07aac'


let privateKey =null;
let signerPublicKey =null;
let batcherPublicKey = null

const setBatcherPubkey = () => {
    return api.getInfo('info');
}


const createTxn = payload => {
  console.log('payload inside createTxn is')
    console.log(payload)
  const header = TransactionHeader.encode({
    signerPublicKey,
    batcherPublicKey,
    familyName: FAMILY_NAME,
    familyVersion: FAMILY_VERSION,
    inputs: [NAMESPACE],
    outputs: [NAMESPACE],
    nonce: (Math.random() * 10 ** 18).toString(36),
    payloadSha512: createHash('sha512').update(payload).digest('hex'),
  }).finish()

  return Transaction.create({
    payload,
    header,
    headerSignature: context.sign(header, privateKey)
  })
}

const encodeTxns = transactions => {
  return TransactionList.encode({ transactions }).finish()
}



const submit = async (payloads, wait = false,privateKeyHex) => {
    if (!_.isArray(payloads)){
        payloads = [payloads]
    }

    try{
      let response = await setBatcherPubkey();
      if(response.data ){
          batcherPublicKey= response.data.pubkey;
      }
      privateKey=secp256k1.Secp256k1PrivateKey.fromHex(privateKeyHex);
      signerPublicKey = context.getPublicKey(privateKey).asHex();

    }

    catch (err){
        throw err;

    }

    let result;
    try{
        const txns = payloads.map(payload => createTxn(payload));
        const txnList = encodeTxns(txns);

        let response = await api.postBinary(`transactions${wait ? '?wait' : ''}`, txnList);
        result = response.data;
        console.log(result)
    }
    catch (error){
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx

            throw error.response.data;
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            throw error.request;
        } else {
            // Something happened in setting up the request that triggered an Error
            throw error.message;
        }

    }

    return result;

}

module.exports = {
  submit
}
