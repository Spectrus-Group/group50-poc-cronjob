
'use strict'
const appendQuery= require('append-query');
const qs = require('qs');
const axios= require('axios');
const async= require('async');


const instance = axios.create({
    baseURL: 'https://ppt-api.thinaer.io/v1'
});

const config={
    apiUrl:'http://localhost:3000/api/v1',
    privateKey:'6be01933525f4da1f55f47a65fc4da114df0b6b542baba53558adfa240b8a16c',
    clientUsername:'5j5q72l29mqum4ls84him4cbqr',
    clientPassword:'l5gcjau5okonui0ap6fd9lhmtpql5nqdla5p75oggf4608d1413',
    clientAuthUrl:'https://thinaer.auth.us-west-2.amazoncognito.com/oauth2/token',
    appId:'5aa19a708255f653fc67fc58',
    accessToken:null,
    lastTimestamp:null,
    expiresIn:3600,
    pollInterval:1000*60*6
}




const getToken= async ()=>{
    let result;
    let auth = 'Basic ' + Buffer.from(config.clientUsername + ':' + config.clientPassword).toString('base64');

    try{
        let response = await axios.post(
            config.clientAuthUrl,
            qs.stringify({ 'grant_type': 'client_credentials' }),
            {
                headers:{
                    'Content-Type':'application/x-www-form-urlencoded',
                    'Authorization':auth
                }
            })

        result= response.data || {} ;
        console.log(result)
        config.accessToken=result.access_token;
        config.lastTimestamp=Date.now();
        config.expiresIn=result.expires_in;

        instance.defaults.headers.common['Authorization'] = config.accessToken;
        instance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    }
    catch (err){
        throw new Error(err);
    }

    return result;

};


const getPeripheral = (assetId)=>{
    return instance.get(`${config.appId}/things/${assetId}/peripherals?limit=1`);
}


const getRecords= ()=>{
    return axios.get(config.apiUrl+'/records');
}


const updateProperties = (prop)=>{
    console.log('updating prop are')
    console.log(prop)
    return axios.post(config.apiUrl+'/updateProperties',
        {
            temperature:prop.internalTemperature,
            privateKey:config.privateKey,
            assetId:prop.assetId
        })
}

const init= async ()=>{
    let records=[];
    try{
        await getToken();
    }

    catch (err){
        throw new Error(err);
    }

    try {
        let response = await getRecords();
        records= response.data || [];
    }

    catch (err){
        console.log(err)
        throw  new Error(err);
    }


    if(records.length){
        console.log('total records are', records.length);
        async.each(records, (item,cb)=>{

            getPeripheral(item.recordId).then((response)=>{
                console.log(response.data)
                let latestRecodedValues= response.data.docs || [];
                console.log(latestRecodedValues)

                let arrayOfTemperatures = item.updates.properties.temperature;

                let ts1=0, ts2=0,indexWithRecentTimeStamp=0;

                if(arrayOfTemperatures.length>0){
                    console.log(arrayOfTemperatures[arrayOfTemperatures.length-1]);
                    ts1 = arrayOfTemperatures[arrayOfTemperatures.length-1].timestamp;

                }
                for(let i =0;i<latestRecodedValues.length;i++){
                    if(new Date(latestRecodedValues[i].reading.updatedAt).getTime() > ts2){
                        ts2= new Date(latestRecodedValues[i].reading.updatedAt).getTime();
                        indexWithRecentTimeStamp=i;
                    }
                }


                if(ts2 >ts1 && latestRecodedValues.length){
                    let record= latestRecodedValues[indexWithRecentTimeStamp].reading || {};
                    record.assetId= item.recordId;
                    updateProperties(record).then((resp)=>{
                        console.log('property has been updated for', item.recordId)
                        return cb(null)
                    }).catch((err)=>{
                        return cb(null);
                    })
                }else {
                    return cb(null);
                }

            }).catch((err)=>{
                return cb(null);

            })




        },(err)=>{
            console.log('final callback')
            console.log(err);
        })
    }
    else {
        console.log('no records found');
    }
}

setInterval(()=>{
    init().then(()=>{

    }).catch((err)=>{
        console.log(err.message)

    })
},config.pollInterval)
